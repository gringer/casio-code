# TIME
Used for debugging time; shows the number of loops that have passed when a key is pressed
```
Lbl 1
0 -> A
Do
Getkey -> Z
LpWhile Z != 0
While Z = 0
Do
Getkey -> Z
A + 1 -> A
WhileEnd
A/
Goto 1
```
# SLIDER
There was a collection of minigames in a program called ThinkFast. I copied some of them. For this game, you are presented with a number of columns in which an object will appear, then should press a key corresponding to the correct column. If the object appears on its own, then the correct column is the column that the object is in; if the object appears together with a 'π', then the correct column is... er... I'm not sure.
```
0 -> M
0 -> T
0 -> R
Lbl 1
ClrText
Locate 1,1,""
Getkey -> Z
WhileEnd
Int (Ran# * 4) + 1 -> A
Int (Ran# * 2) + 1 -> B
B != 1 => Locate 10,3,"π"
Locate A,1,"O"
0 -> I
Getkey -> Z
While Z = 0
Do
I + 1 -> I
Getkey -> Z
WhileEnd
ClrText
R + I -> R
T + 1 -> T
0 -> H
Int (Z / 10) = 7 => 1 -> H
Z = 47 => Prog "STATS"
Z = 47 => Stop
Z = 72 => 0 -> H
If H = 0
Then Z - 32 -> Z
Z / 10 -> Z
5 - Z -> Z
Else Z - 70 -> Z
9 - Z -> Z
IfEnd
(Z != A) Or (H + 1 != B) => Prog "MISS"
Goto 1
```

# KEY
I used this code to debug keypresses. It shows on the screen the kecode of keys that are pressed.
```
Lbl 1
Locate 1,1,"  "
Locate 1,1,Getkey
Goto 1
```

# MISS
Shows "MISSED" on the screen. The commented code fills the screen with "MISSED".
```
ClrText
M + 1 -> M
'For 0 -> K To 1
"MISSED"
'Next
```

# STATS
Shows statistics on the screen for the "SLIDER" game (when the player presses the 'EXIT' key). The odd numbers used here are my attempt at approximately calibrating loop times and converting into seconds.
```
T - 1 -> T
ClrText
Locate 1,1,"TOTAL"
Locate 1,2,T
1 -> P
T != 0 => M / T -> P
Int (P * 100) -> P
Locate 1,3,"MISSED"
Locate 1,4,P
Locate 4,4,"PCT"
T = 0 => 1 -> T
R / T -> P
If P > 390
Then 6.2E-4 * P^1.61 -> P
Else 3.784E-5 * P^2 + 9.4626E-3 + 0.35 -> P
P * 2 -> P
IfEnd
Int (P * 10) / 10 -> P
Locate 1,5,"AVE REACTION TIME"
Locate 1,6,P
If P = 0
Then Locate 1,6,"0"
1 -> P
IfEnd
Locate 3 + Int(log P) + 2 * Int(P - Int P + 0.9),6,"S"
```

# SIDE
This is another ThinkFast minigame. In this one, an 'X' flashes on the screen, then is covered up by an array of 'X's, four on the left and four on the right. If you correctly choose the correct side where the 'X' flashed, then you get a point, and the time between the flash and covering up decreases.
```
0 -> M
200 -> D
Lbl 1
ClrText
Int (Ran# * 2) * 5 -> A
Int (Ran# * 4) + 1 -> B
Locate A + B,4,"X"
For 1 -> C To D
Next
Locate 1,4,"XXXX XXXX"
Prog "GK"
2 -> F
Z = 38 => 0 -> F
Z = 27 => 5 -> F
D - 5 -> D
A = F => Goto 1
D + 12 -> D
Prog "MISS"
Goto 1
```

# GK
This function gets a key press (first waiting until a key has not been pressed, if necessary), then stores the result into variable 'Z'
```
1 -> Z
While Z != 0
Getkey -> Z
WhileEnd
While Z = 0
Getkey -> Z
WhileEnd
```

# STAR
This is another ThinkFast minigame, where you are presented with a star that is either filled or unfilled. If the star is unfilled, then there is a short delay before the star becomes filled. Then the program waits for a response for a short while. If the star is initially unfilled, then the correct response is nothing (i.e. no keypresses); if the star is initially filled, then a key should be pressed. In this implementation, the star is represented by an "O".
```
50 -> F
Lbl 1
Int (Ran# * 21) + 1 -> A
Int (Ran# * 7) + 1 -> B
Int (Ran# * 2) -> C
Ran# * 1000 -> D
For 1 -> Y to D
Next
0 -> D
If C = 0
Then Locate A,B,"O"
F -> D
For 1 -> Y to D
Getkey -> Z
Z != 0 => Prog "CHSTAR"
Next
IfEnd
Locate A,B,"0"
(200 - D) -> D
For 1 -> Y To D
Getkey -> Z
Z != 0 => Prog "CHSTAR"
Next
Locate A,B," "
Goto 1
```

# CHSTAR
Supplementary program to "STAR"; checks to see if the correct response has been made after a keypress.
```
F - 1 -> F
If C = 0
Then ClrText
"WRONG"
F + 2 -> F
ClrText
IfEnd
1000 -> Y
```

# MASTER
Implementation of the game "Mastermind". Five numbers are presented, which can be changed by pressing a key in the column associated with the row of the number, and the guess is submitted by pressing 'EXE'. After submitting a guess, two numbers appear below the guess, one showing the number of correct guesses in the correct location, and another showing the number of correct guesses in the wrong location. The game is won when all five positions and numbers are correctly guessed.
```
Lbl 0
ClrText
[[0,0,0,0,0]] -> Mat A
[[0,0,0,0,0]] -> Mat B
[[0,0,0,0]] -> Mat C
1 -> I
For 1 -> A To 5
Int (Ran# * 4) -> Mat B[1,A]
Next
Prog "DRAWMAS"
Lbl 1
Prog "GK"
If Z = 31
Then Prog "CHECKMAS"
Locate I,6,R
Locate I,7,D
I + 1 -> I
I > 20 => 1 -> I
If R = 5
Then ClrText
Locate 1,1,"YOU WIN"
Prog "GK"
Prog "PAG"
Z = 0 => Stop
Goto 0
IfEnd
Int ((Z - 20) / 10) -> Z
If Z > 0
Then Frac ((Mat A[1,6-Z]+1) / 4) * 4 -> Mat A[1,6-Z]
IfEnd
Prog "DRAWMAS"
Goto 1
```

# DRAWMAS
Supplementary program to "MASTER". Draws the current guess.
```
For 1 -> Y To 5
Mat A[1,Y] -> Z
Locate I, Y, (9-Z)
Next
```

# CHECKMAS
Supplementary program to "MASTER". Checks the current guess and updates the clues.
```
0 -> R
0 -> D
0 * Mat C -> Mat C
For 1 -> Y To 5
Mat B[1,Y] + 1 -> X
Mat C[1,X] + 1 -> Mat C[1,X]
Next
For 1 -> Y To 5
Mat A[1,Y] + 1 -> X
If X = Mat B[1,Y] + 1
Then R + 1 -> R
Mat C[1,X] - 1 -> Mat C[1,X]
IfEnd
Next
For 1->Y To 5
Mat A[1,Y] + 1 -> X
If (Mat B[1,Y] + 1 != X) And (Mat C[1,X] > 0)
Then Mat C[1,X] - 1 -> Mat C[1,X]
D + 1 -> D
IfEnd
Next
```

# PAG
Supplementary program to "MASTER". Asks the player if they want to play again, storing 1 in Z if the response is yes, and 0 in Z otherwise.
```
ClrText
Locate 5,1,"DO YOU WANT"
Locate 4,2,"TO PLAY AGAIN?"
Locate 5,3,"Press [EXE]"
Prog "GK"
If Z=31
Then 1 -> Z
Else 0 -> Z
IfEnd
ClrText
Z -> Z
```

# RPUT
Repeatedly places an "X" in a random location on the screen.
```
Lbl 1
Locate Int (Ran# * 21) + 1, Int (Ran# * 7) + 1, "X"
ClrText
Goto 1
```

# XANDO
Noughts and Crosses game. Positions are entered via the number pad (i.e. numbers 1-9, matching the 3x3 noughts and crosses grid).
```
ViewWindow -2.15, 4.15, 0, -0.55, 2.55, 0, 0, 2π, 2π / 8
[[0,0][0,0][0,0][0,0][0,0][0,0][0,0][0,0][0,0][0,0]] -> Mat A
AxesOff
GridOff
ClrText
"PLAYERS" ? -> P
P = 1 => "GO FIRST" ? -> F
Lbl 0
0 -> M
0 -> Q
Cls
Prog "DRAWBRD"
Text 1,1,"WAIT"
PxlOn 22,40
'Prog "DRAWALL"
Text 1,1,"     "
PxlOn 22,40
1 -> W
Lbl 1
If (P = 2) Or ((P != 0) And (2 - W = F))
Then Prog "GK"
Else Prog "AI4"
IfEnd
Z - 52 -> Z
Int (Z / 10) -> A
Frac (Z / 10) * 10 -> B
(A > 2) Or (B > 2) Or (A < 0) Or (B < 0) => Goto 1
2 - A -> A
Prog "DECODE"
Z != 0 => Goto 1
M + (2^(B * 6 + A * 2)) * W -> M
Lbl 2
Prog "DRAWPNT"
3 - W -> W
Q + 1 -> Q
Prog "CHKWIN"
Z = 0 => Goto 1
Prog "GK"
Goto 0
```

# DRAWBRD
Supplementary program to "XANDO". Draws the base of the current board.
```
Cls
F-Line 0.5,-0.5,0.5,2.5
F-Line 1.5,-0.5,1.5,2.5
F-Line -0.5,0.5,2.5,0.5
F-Line -0.5,1.5,2.5,1.5
```

# DECODE1 [B]
Supplementary program to "DECODE". Does a bitwise AND of M and Z.
```
MandZ->Z
```

# DECODE
Supplementary program to "XANDO". Puts a value in Z: the bits of M at point A,B.
```
B * 6 + A * 2 -> C
2^C + 2^(C+1) -> Z
Prog "DECODE1"
Z / (2^C) -> Z
```

# CIRCLE
Supplementary program to "XANDO". Draws a circle at A,B.
```
Graph(X,Y)=(0.4 sin T + A, 0.4 cos T + B)
```

# CROSS
Supplementary program to "XANDO". Draws a cross at A,B.
```
F-Line A-0.28, B-0.28, A+0.28, B+0.28
F-Line A-0.28, B+0.28, A+0.28, B-0.28
```

# DRAWALL
Supplementary program to "XANDO". Draws the relevant item at all points in the grid.
```
For 0 -> A To 2
For 0 -> B To 2
Prog "DRAWPNT"
```

# DRAWALL
Supplementary program to "DRAWALL". Draws the relevant item at a single point in the grid. Z stores 1 for a circle, 2 for a cross, and 3 for both.
```
Prog "DECODE"
(Z = 1) Or (Z = 3) => Prog "CIRCLE"
(Z = 2) Or (Z = 3) => Prog "CROSS"
```

# CHKWIN1 [B]
Supplementary program to "CHKWIN". This is an encoding of all game finish states.
```
0 -> Z
Q = 9 => 35 -> Z
M and 21 = 21 => 1 -> Z
M and 4161 = 4161 => 3 -> Z
M and 65793 = 65793 => 5 -> Z
M and 16644 = 16644 => 7 -> Z
M and 4368 = 4368 -> 9 -> Z
M and 66576 = 66576 -> 11 -> Z
M and 1344 = 1344 -> 13 -> Z
M and 86016 = 86016 => 15 -> Z
M and 42 = 42 => 2 -> Z
M and 8322 = 8322 => 6 -> Z
M and 131586 = 131586 => 10 -> Z
M and 33288 = 33288 => 14 -> Z
M and 8736 = 8736 => 18 -> Z
M and 133152 = 133152 => 22 -> Z
M and 2688 = 2688 => 26 -> Z
M and 172032 = 172032 => 30 -> Z
```

# CHKWIN
Supplementary program to "XANDO". Checks to see if the game has been won by either player, or ended in a draw state. Draws a line if the game has been won, and displays the winner.
```
`Z = 0 => Goto 1
'SOMEONE WON, CAN CHANGE W
1 -> W
Z = 35 => 3 -> W
If Frac (Z / 2) = 0
Then 2 -> W
Z / 2 -> Z
IfEnd
Z = 1 => F-Line 0,0,2,0
Z = 3 => F-Line 0,0,0,2
Z = 5 => F-Line 0,0,2,2
Z = 7 => F-Line 1,0,1,2
Z = 9 => F-Line 2,0,0,2
Z = 11 => F-Line 2,0,2,2
Z = 13 => F-Line 0,1,2,1
Z = 15 => F-Line 0,2,2,2
W = 1 => Text 1,1,"CIRCLE WINS"
W = 2 => Text 1,1,"CROSS WINS"
W = 3 => Text 1,1,"DRAW"
Plot 1,1
Lbl 1
```

# AI0
Supplementary program to "XANDO". Very dumb AI; predictable path.
```
Text 1,1,"AIL0"
PxlOn 22,40
For 0 -> A to 2
For 0 -> B to 2
Prog "DECODE"
If Z = 0
Then ((2 - A) * 10 + B) + 52 -> Z
Text 1,1,"     "
2 -> A
2 -> B
IfEnd
Next
Next
```

# AI1
Supplementary program to "XANDO". Low AI; random position.
```
Text 1,1,"AIL1"
PxlOn 22,40
1 -> Z
While Z != 0
Int (Ran# * 3) -> A
Int (Ran# * 3) -> B
Prog "DECODE"
WhileEnd
((2 - A) * 10 + B) + 52 -> Z
Text 1,1,"     "
PxlOn 22,40
```

# AI2
Supplementary program to "XANDO". Medium AI; completes own lines.
```
Text 10,1,"AIL2"
PxlOn 22,40
0 -> R
If Q >= 4
Then For 0 -> A To 2
For 0 -> B to 2
Prog "DECODE"
If Z = 0
Then M + (2^(B * 6 + A * 2) * W) -> M
Prog "CHKWIN1"
M - (2^(B * 6 + A * 2) * W) -> M
If Z != 0
Then 1 -> R
IfEnd
IfEnd
If R != 0
Then (2 - A) * 10 + B + 52 -> Z
2 -> A
2 -> B
IfEnd
Next
Next
IfEnd
R = 0 => Prog "AI1"
Text 10,1,"      "
PxlOn 22,40
```

# AIL3
Supplementary program to "XANDO". High AI; blocks opponent. It does this by acting as if it were the other player, and seeing if the game is winnable using AIL2. If so, then it does that winning move, resulting in a block.
```
Text 20,1,"AIL3"
PxlOn 22,40
Prog "AI2"
If (R = 0) And (Q >= 3)
Then 3 - W -> W
Q + 1 -> Q
Prog "AI2"
3 - W -> W
Q - 1 -> Q
IfEnd
Text 20,1,"     "
PxlOn 22,40
```

# AIL4
Supplementary program to "XANDO". Very high AI; predetermined win or draw.
```
Text 30,1,"AIL4"
PxlOn 22,40
0 -> D
'DECIDED
If Q = 0
Then 72 -> Z
1 -> D
0 -> H
'TYPE
IfEnd
If Q = 1
Then 0 -> H
If (Frac ((A - B) / 2) = 0) And (A != 1)
Then 63 -> Z
1 -> D
B * 3 + A + 1 -> H
IfEnd
If (A = 1) And (B = 1)
Then 72 -> Z
1 -> D
2 -> H
IfEnd
If Abs (A-B) = 1
Then 5 -> H
If AB = 0
Then 72 -> Z
1 -> D
IfEnd
If AB != 0
Then 54 -> Z
1 -> D
IfEnd
IfEnd
IfEnd
If Q = 2
Then If (Abs (A - B) = 1)
Then 63 -> Z
1 -> D
If (B = 0)
Then 1 -> H
IfEnd
If (A = 0)
Then 2 -> H
IfEnd
IfEnd
If (A = 2) And (B = 2)
Then 52 -> Z
1 -> D
IfEnd
If (A = 1) And (B = 1)
Then 54 -> Z
1 -> D
IfEnd
If (Abs (A - B) = 2)
Then 54 -> Z
1 -> D
IfEnd
IfEnd
A -> I
B -> J
If D = 0
Then Prog "AI3"
IfEnd
I -> A
J -> B
If (Q = 3) And (R = 0) And (D = 0) And (H = 5)
Then 1 -> A
1 -> B
Prog "DECODE"
If Z = 0
Then 63 -> Z
1 -> D
IfEnd
IfEnd
If (Q = 3) And (R = 0) And (D = 0) And (H = 2)
Then If (A = 2) And (B = 2)
Then 0 -> B
Prog "DECODE"
If Z = 0
Then 52 -> Z
IfEnd
IfEnd
IfEnd
If (Q = 3) And (R = 0) And (D = 0) And (H != 0)
Then If H = ((2 - B) * 3 + (2 - A) + 1)
Then 1 -> A
0 -> B
Prog "DECODE"
If Z = 0
Then 62 -> Z
1 -> D
IfEnd
IfEnd
'MY ADD GOES HERE; ILOOP POTENTIAL DOESN'T CHECK BLANK (NO DECODE)
If (D = 0) And (Frag (H / 2) != 0)
Then If (H = 9) And (2 - A - B = 1)
Then 74 -> Z
IfEnd
If (H = 7) And (A - B = 1)
Then 54 -> Z
IfEnd
If (H=3) And (A - B = -1)
Then 72 -> Z
IfEnd
If (H = 1) And (2 - A - B = -1)
Then 74 -> Z
IfEnd
1 -> D
IfEnd
IfEnd
If (Q = 4) And (R = 0) And (H = 0)
Then 0 -> A
2 -> B
PxlOn 60,5
Prog "DECODE"
If Z=0
Then 74 -> Z
1 -> D
IfEnd
IfEnd
If (Q = 4) And (D = 0) And (R = 0)
Then If H = 1
Then 0 -> A
2 -> B
PxlOn 60,10
Prog "DECODE"
If Z = 0
Then 74 -> Z
1 -> D
IfEnd
IfEnd
If H = 2
Then 2 -> A
0 -> B
PxlOn 60,15
Prog "DECODE"
If Z = 0
Then 52 -> Z
IfEnd
IfEnd
IfEnd
Text 30,1,"     "
PxlOn 22,40
```

# CONNECT4
# DRAW4BRD
# DECODEC4
# DRAW4ALL
# DRAW4PNT
# CHKWIN4
# AND4
# CHASE
# LAUNCH
# NUMS
# DICE
# DICEST
# LLOYD
# LLOYDP
# ROTA
# DRAWRO
# ROTMAT
# ROPOS
# RANDSQR
# CHKSQR
# PTCHR
# MEMORY
# CURSOR
# ROTMAT2
# BOUNCE
# SETEMPTY [B]
# MATEMPTY
# DRAW3D
# TEST3D
# CLUSTERC
# CLUSTERV
# MOD
# YEAR
# ALPHA
