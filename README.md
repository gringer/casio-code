# CasioCode

A project to store code from a CASIO CFX-9850G. I bought this calculator during my high school years as a replacement for a black & white CASIO programmable calculator. During my downtime at school, I wrote a few games to play on it. Many of these were written during computer science lectures at university. The noughts and crosses game is my particular favourite, featuring four levels of "AI", the highest level of which is impossible to win against.

The code detailed here is not directly runnable; it serves only as a record of what I have written, so that I don't have too much data loss if the calculator gets lost or loses too much battery for too long a time.

## Licence

GPL-v3, Copyright David Eccles, 1999-2024. 
